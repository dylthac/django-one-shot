from django.contrib import admin

from todos.models import TodoItem, TodoList

# Register your models here.


admin.site.register(TodoList)
admin.site.register(TodoItem)
