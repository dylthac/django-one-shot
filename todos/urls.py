from django.urls import path

from todos.views import (
    to_do_detail,
    todo_create,
    todo_delete,
    todo_edit,
    todoitem_create,
    todolist,
)


urlpatterns = [
    path("", todolist, name="todo_list_list"),
    path("<int:pk>/", to_do_detail, name="todo_list_detail"),
    path("create/", todo_create, name="todo_list_create"),
    path("<int:pk>/edit", todo_edit, name="todo_list_update"),
    path("<int:pk>/delete", todo_delete, name="todo_list_delete"),
    path("items/create/", todoitem_create, name="todo_item_create"),
]
