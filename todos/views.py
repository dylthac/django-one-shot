from msilib.schema import ListView
from django.shortcuts import get_object_or_404, redirect, render
from todos.forms import TodoItemForm, TodoListForm

from todos.models import TodoList
from django.views.generic.detail import DetailView

# Create your views here.


def todolist(request):
    todolists = TodoList.objects.all()
    context = {"todolist_list": todolists}
    return render(request, "todotemplates/list.html", context)


def to_do_detail(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)
    context = {"todolist": todolist}
    return render(request, "todotemplates/detail.html", context)


def todo_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.pk)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todotemplates/create.html", context)


def todo_edit(request, pk):
    todolist = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", todolist.pk)
    else:
        todolist = get_object_or_404(TodoList, pk=pk)
        form = TodoListForm(instance=todolist)
    return render(request, "todotemplates/edit.html", {"form": form})


def todo_delete(request, pk):
    context = {}
    obj = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("todo_list_list")
    return render(request, "todotemplates/delete.html", context)


def todoitem_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    return render(request, "todo_items/create.html", {"form": form})
