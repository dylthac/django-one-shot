* [x] Fork and clone the starter project from django-one-shot 
    * [x] git clone <<fork_url>>
* [x] Create a new virtual environment in the repository directory for the project
  * [ ] python -m venv .venv
* [x] Activate the virtual environment
  * [ ] .\.venv\Scripts\Activate.ps1    
* [x] Upgrade pip
  * python -m pip install --upgrade pip
* [x] Install django
  * [x] pip install django
* [x] Install black
  * [x] pip install black
* [x] Install flake8
  * [x] pip install flake8
* [x] Install djlint
  * [x] pip install djlint
* [] Deactivate your virtual environment
* [] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
* [x] Create django project
  * [x] django-admin startproject <<name>> .
* [x] create django app
  * [ ] python manage.py startapp <<name>>


## Models
* [x] make migrations
  * [ ] python manage.py makemigrations
* [x] 

